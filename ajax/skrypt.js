document.addEventListener('DOMContentLoaded', function() {
  

const body = document.querySelector("body");
console.log(body);

fetch("https://www.googleapis.com/books/v1/volumes?q=subject+Wied%C5%BAmin").then(resp => {if (resp.ok) {
    return resp.json()
} else {
    throw new Error("Wystąpił błąd połączenia!")
};
}).then(resp => {
    console.log(resp);
    for(item of resp.items){
        let div = document.createElement("div");
        div.classList.add("book");
        div.innerHTML=`
        <div class="content">
        <h1>${item.volumeInfo.title}</h1>
        <h3>${item.volumeInfo.authors || " - "}</h3>
        <h4>${item.volumeInfo.pageCount ? item.volumeInfo.pageCount + " stron" : ""} ${item.accessInfo.pdf.isAvailable == true ? "| PDF dostępny" : "" }</h4>
        </div>
        <div class="button">
        <a href="${item.volumeInfo.previewLink}">Podgląd</a>
        </div>
        `;
        body.appendChild(div);
        

    }
})
.catch(error => console.dir("Błąd: ", error));

});
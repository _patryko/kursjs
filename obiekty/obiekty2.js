const prod1 = {
    name : "Produkt pierwszy",
    price : 5,
    weight : 1.5
}

const prod2 = {
    name : "Produkt drugi",
    price : 10,
    weight : 2.44
}

console.log(prod1);
console.log(prod2);

console.log("Produkt numer jeden to: " + prod1.name);
console.log("Produkt numer dwa to: " + prod2.name);

console.log("Produkty kosztują razem: " + (prod1.price + prod2.price));
console.log("Produkty ważą razem: " + (prod1.weight + prod2.weight));

//////////////////////////////////////

const currentUser = {
name:"MyName",
surname:"MySurname",
email:"MyEmail",
www:"MyWWW",
userType:null,
show : function() {
    console.log("Imie: ", this.name);
    console.log("Nazwisko: ", this.surname);
    console.log("Email: ", this.email);
    console.log("WWW: ", this.www);
    console.log("UserType: ", this.userType);
}

}

currentUser.show();

//////////////////////////////////////

const book = {
    title:"MyBook",
    author:"MyAuthor",
    pageCount:33,
    publisher:"MyPublisher",
    showDetails : function(){
        for(const i in book){
            if(i !== "showDetails"){
            console.log("Klucz: ", i);
            console.log("Wartość: ", this[i]);
            }
        }
    }
    }

    book.showDetails();

//////////////////////////////////////

const users = [
    //id name surname email age value
    [  1, "Shauna", "Bradnocke", "sbradnocke0@altervista.org", 20, 108.28 ],
    [  2, "Mela", "Redman", "mredman1@nps.gov", 24, 267.37 ],
    [  3, "Othelia", "Lemon", "olemon2@slashdot.org", 15, 748.06 ],
    [  4, "Meier", "Cockell", "mcockell3@icio.us", 32, 1951.64 ],
    [  5, "Shellysheldon", "Gronowe", "sgronowe4@cnbc.com", 16, 1040.54 ],
    [  6, "Francisca", "Tofanini", "ftofanini5@gnu.org", 21, 1544.08 ],
    [  7, "Cliff", "Underwood", "cunderwood6@addtoany.com", 10, 451.21 ],
    [  8, "Caron", "Falshaw", "cfalshaw7@hugedomains.com", 27, 1968.72 ],
    [  9, "Anitra", "Warters", "awarters8@intel.com", 12, 380.68 ],
    [ 10, "Caitrin", "Baudrey", "cbaudrey9@ihg.com", 13, 1385.44 ]
]

function fixData(tab){
    let tab2 = []

    for(t of users)
    {
        const user = {
            id: t[0],
            name : t[1],
            surname : t[2],
            email : t[3],
            age : t[4],
            money : t[5]
        }
    
    tab2.push(user);
    }

    return tab2;
}

const fixTab = fixData(users);
console.dir(fixTab);


//////////////////////////////////////

const summary = {
    money:0,
    age:0,
    avarageAge:0
}

for(ft of fixTab){
    summary.money+=ft["money"];
    summary.age+=ft["age"];
}

summary.avarageAge = summary.age/fixTab.length;

console.dir(summary);

//////////////////////////////////////

function Car(name,brand,engine,mile,age){
    this.name = name;
    this.brand = brand;
    this.engine = engine;
    this.mile = mile;
    this.age = age;
}

Car.prototype.printDetails = function(){
    console.log("Name: " + this.name);
    console.log("Brand: " + this.brand);
    console.log("Engine: " + this.engine);
    console.log("Mile: " + this.mile);
    console.log("Age: " + this.age);
}

const car1 = new Car("Panda","Fiat",1.0,84321,10);
const car2 = new Car("Tipo","Fiat",1.3,23321,10);
const car3 = new Car("206","Peugeot",1.4,102321,12);

car1.printDetails();
car2.printDetails();

//////////////////////////////////////

function Enemy(name,speed,attack,posX){
    this.name=name;
    this.live = 5;
    this.speed=speed;
    this.attack=attack;
    this.posX=posX;
}

Enemy.prototype.move = function(){
    this.posX-=this.speed;
    console.log("Jestem " + this.name + ". Znajduje sie na pozycji " + this.posX);
}

Enemy.prototype.attackEnemy = function(){
    console.log("Jestem " + this.name  + ". Atakuje gracza z pozycji " + this.posX + " z sila " + this.attack);
}

Enemy.prototype.hit = function(){
    this.live-=1;
    console.log("Jestem " + this.name + ". Mam teraz zycia " + this.live);

}

const enemy1 = new Enemy("Jan",5,5,10);
const enemy2 = new Enemy("Pawel",10,10,20);

enemy1.move();
enemy1.move();
enemy1.move();
enemy2.move();
enemy2.move();
enemy1.attackEnemy();
enemy2.attackEnemy();
enemy2.hit();
enemy2.hit();

//////////////////////////////////////

String.prototype.sortText = function(x){
    return this.split(x).sort().join(x);
}

const test = "KOT|Ma|Ale|Ela";
console.log(test.sortText("|"));

//////////////////////////////////////

const obiekt = {
    time:2000,
    pets: ["kot","pies","mysz"],
    print: function(){
        setTimeout(() => this.pets.forEach((pet) => console.log(pet.toUpperCase())
        ),this.time);
    }
}

obiekt.print();
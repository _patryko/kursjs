const media = window.matchMedia("(min-width: 500px)");
const myDiv = document.querySelector("div");

clickMe = function(){
    console.log("Kliknięto");
}

function setBg(media) {
    if (media.matches) {
        myDiv.removeEventListener("click",clickMe);
        myDiv.style.setProperty('background-color', 'red');
        myDiv.addEventListener("click",clickMe);
    } else {
        
        myDiv.style.setProperty('background-color', 'blue');
        myDiv.removeEventListener("click",clickMe);
    }
}

setBg(media);
media.addListener(setBg);

const images = document.querySelectorAll("img");
console.log(images.length);

images.forEach(function(item,index){
    console.log(item);
    item.setAttribute("data",item.src);
    item.removeAttribute("src");
    item.classList.add("loading");

    const im = new Image();

    im.addEventListener('load',function(){
        item.classList.remove("loading");
    });
    im.src = item.getAttribute("data");
    item.src = im.src;

});
const paint = {
    changeSize : function(e) {
        //wartość wyświetlana przy input:range
        this.sizeElemVal.innerText = e.target.value;
        //zmieniamy wielkość rysowania
        this.ctx.lineWidth = e.target.value;
        this.ctx2.lineWidth = e.target.value;
    },

    changeColor : function(e) {
        //zmieniamy kolor rysowania
        const color = this.colorElem.value;
        this.ctx.strokeStyle = color;
        this.ctx2.strokeStyle = color;

    },
    enableMode : function(mode) {
        //sprawdzamy czy włączany tryb jest poprawny
        if (this.avaibleMode.indexOf(mode) !== -1) {
            this.mode = mode;
        }
    },
    mouseEnable : function(e) {
        this.canDraw = true;

        const mousePos = this.getMousePosition(e);
        this.startX  = mousePos.x;
        this.startY = mousePos.y;

        this.ctx.beginPath();
        this.ctx.moveTo(this.startX, this.startY);
    },

    mouseDisable : function(e) {
        this.canDraw = false;

        if (this.mode === 'line' || this.mode === "rectangle" || this.mode === "circle" || this.mode === "triangle") {
            //klonujemy canvas2 na 1
            this.ctx.drawImage(this.canvasElem2, 0, 0);
            //czyścimy 2 canvas
            this.ctx2.clearRect(0, 0, this.canvasElem2.width, this.canvasElem2.height);
        }
    },
    getElementPos: function(obj) {
        let top = 0;
        let left = 0;
        while (obj && obj.tagName != "BODY") {
            top += obj.offsetTop - obj.scrollTop;
            left += obj.offsetLeft - obj.scrollLeft;
            obj = obj.offsetParent;
        }
        return {
            top: top,
            left: left
        };
    },

    mouseMove : function(e) {
        if (this.canDraw) {
            const mousePos = this.getMousePosition(e);

            if (this.mode === 'draw') {
                this.ctx.lineTo(mousePos.x, mousePos.y);
                this.ctx.stroke();
            }
            if (this.mode === 'line') {
                //w każdej klatce czyścimy canvas2
                this.ctx2.clearRect(0, 0, this.canvasElem2.width, this.canvasElem2.height);
                this.ctx2.beginPath();
                //rysujemy linię od początkowej pozycji
                this.ctx2.moveTo(this.startX, this.startY);
                //do aktualnej pozycji kursora
                this.ctx2.lineTo(mousePos.x, mousePos.y);
                this.ctx2.closePath();
                this.ctx2.stroke();
            }
            if (this.mode === 'rectangle') {
                this.ctx2.clearRect(0, 0, this.canvasElem2.width, this.canvasElem2.height);
                this.ctx2.beginPath();
                this.ctx2.moveTo(this.startX, this.startY);
                this.ctx2.rect(this.startX, this.startY, mousePos.x-this.startX, mousePos.y-this.startY);
                this.ctx2.closePath();
                this.ctx2.stroke();
            }
            if (this.mode === 'circle') {
                this.ctx2.clearRect(0, 0, this.canvasElem2.width, this.canvasElem2.height);
                let radius = Math.sqrt(Math.pow(((mousePos.x)-this.startX),2)+Math.pow(((mousePos.y)-this.startY),2))/2;
                
                this.ctx2.moveTo(this.startX, this.startY);
                this.ctx2.beginPath();

                
                this.ctx2.arc(this.startX+((mousePos.x-this.startX)/2), this.startY+((mousePos.y-this.startY)/2), radius,0,2 * Math.PI);
                //this.ctx2.closePath();
                this.ctx2.stroke();
               
            }
            if (this.mode === 'triangle') {
                this.ctx2.clearRect(0, 0, this.canvasElem2.width, this.canvasElem2.height);
                

                this.ctx2.beginPath();

                this.ctx2.moveTo(this.startX,mousePos.y);
                this.ctx2.lineTo(this.startX+((mousePos.x-this.startX)/2),this.startY);
                this.ctx2.lineTo(mousePos.x,mousePos.y);
                this.ctx2.lineTo(this.startX,mousePos.y);
                this.ctx2.closePath();
                this.ctx2.stroke();
            }
        }
    },
    // pointsDistance(e){
    //     const mousePos = this.getMousePosition(e);

    //     return Math.sqrt(Math.pow(((mousePos.x-this.startX)-this.startX),2)+Math.pow(((mousePos.y-this.startY)-this.startY),2));
    // },


    getMousePosition : function(e){
        const mouseX = e.pageX - this.getElementPos(this.canvasElem).left;
        const mouseY = e.pageY - this.getElementPos(this.canvasElem).top;

        return {
            x: mouseX,
            y: mouseY
        };
    },
    createCanvas : function() {
        const canvasElem = document.createElement('canvas');
        canvasElem.width = this.canvasCnt.offsetWidth;
        canvasElem.height = this.canvasCnt.offsetHeight;
        return canvasElem;
    },

    setupInitialCtx : function() {
        //tło canvasu
        this.ctx.drawImage(this.img, 0, 0);

        //początkowe ustawienia pędzla
        this.ctx.lineWidth = this.sizeElem.value;
        this.ctx.lineJoin = "round";
        this.ctx.lineCap = "round";
        this.ctx.strokeStyle = this.colorElem.value;

//zaokrąglenia nie musimy tutaj ustawiać
this.ctx2.lineWidth = this.sizeElem.value;
this.ctx2.strokeStyle = this.colorElem.value;

    },

    bindElements : function() {
        //dla każdego elementu przypinamy metody bindem
        //bo chcemy w nich mieć dostęp do naszego obiektu paint
        this.sizeElem.addEventListener('change', this.changeSize.bind(this));
        this.sizeElem.addEventListener('input', this.changeSize.bind(this));

        this.colorElem.addEventListener('change', this.changeColor.bind(this))

        this.canvasCnt.addEventListener('mousemove', this.mouseMove.bind(this));
        this.canvasCnt.addEventListener('mouseup', this.mouseDisable.bind(this));
        this.canvasCnt.addEventListener('mousedown', this.mouseEnable.bind(this));

        let lastMode = "draw";
        //po kliknięciu w przycisk zmiany trybu rysowania
        //wszystkim jego braciom wyłączamy klasę .active, a włączamy tylko temu klikniętemu
        //dodatkowo ustawiamy tryb rysowania na pobrany z dataset.mode klikniętego przycisku
        this.btnsMode.forEach(function(el) {
            el.addEventListener('click', function(e) {
                
                this.mode = e.currentTarget.dataset.mode;
                
                if(this.mode !== "bin"){
                    e.currentTarget.classList.add('active');
                    this.btnsMode.forEach(function(el2) {

                    if (el2 !== e.currentTarget) {
                        el2.classList.remove('active');
                    }
                   
                });
               
                lastMode = this.mode;
                

            }else{
                
                this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
                this.setupInitialCtx();
               
                this.mode=lastMode;
            }
            }.bind(this));
        }, this);
    },

    init : function() {
        this.avaibleMode = ['draw', 'line', 'rectangle'];

        this.img = new Image();
        this.img.addEventListener('load', function() {
            //kontener dla canvasu
            this.canvasCnt = document.querySelector('.paint-canvas-cnt');

            //canvas
            this.canvasElem = this.createCanvas();
            this.canvasCnt.appendChild(this.canvasElem);
            this.ctx = this.canvasElem.getContext('2d');

              //canvas2
              this.canvasElem2 = this.createCanvas();
              this.canvasCnt.appendChild(this.canvasElem2);
              this.ctx2 = this.canvasElem2.getContext('2d');


            this.sizeElem = document.querySelector('.paint-size');
            this.sizeElemVal = document.querySelector('.paint-size-val');
            this.sizeElemVal.innerText = this.sizeElem.value;

            this.colorElem = document.querySelector('.paint-color');

            this.btnsMode = [].slice.call(document.querySelectorAll('.paint-buttons-cnt .button-mode'));

            

            this.btnsMode.filter(function(el){
                return el.dataset.mode === "draw"
            })[0].classList.add('active');

            this.canDraw = false;
            this.mode = 'draw';

            this.setupInitialCtx();
            this.bindElements();


        }.bind(this));
        this.img.src = "canvas-bg.png";
    }
}

paint.init();
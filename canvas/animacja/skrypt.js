document.addEventListener("DOMContentLoaded",function(){
function FpsCtrl(fps, cb) {
    const delay = 1000 / fps;
    let time = null;
    let frame = -1;
    let req;
    let isPlaying = false;

    function loop(timestamp) {
        if (time === null) {
            time = timestamp;
        }
        let seg = Math.floor((timestamp - time) / delay);
        if (seg > frame) {
            frame = seg;

            cb({
                time: timestamp,
                frame: frame
            })
        }
        req = requestAnimationFrame(loop)
    }

    this.start = function() {
        if (!this.isPlaying) {
            this.isPlaying = true;
            req = requestAnimationFrame(loop);
        }
    };

    this.pause = function() {
        if (this.isPlaying) {
            cancelAnimationFrame(req);
            this.isPlaying = false;
            time = null;
            frame = -1;
        }
    };
}


// function draw(ctx) {
    
//     ctx.fillStyle = "#eee";
//     ctx.fillRect(0, 0, canvasElem.width, canvasElem.height);

//     frameNr++;
//     if (frameNr > humanFramesCount) {
//         frameNr = 1;
//     }
//     const frameXpos = (frameNr-1)*frameWidth;
//     //rysujemy Fanthomasa w pozycji 120x35
//     ctx.drawImage(human, frameXpos, 0, frameWidth, frameHeight, 120, 35, frameWidth, frameHeight);
// }

function AnimatedObj(x, y, speed, image, framesCount, ctx,direction) {
    this.x = x;
    this.y = y;
    this.speed = speed;
    this.stepX = this.speed;
    this.stepY = 0;
    this.ctx = ctx
    this.image = image;
    this.direction = direction;

    this.frameNr = 1;
    this.framesCount = framesCount;
    this.frameWidth = this.image.width / this.framesCount;
    this.frameHeight = this.image.height/2;
    let frameYpos = 0;
    if(this.direction == 0) frameYpos = 0;
    else frameYpos = 1;
    

    this.draw = function() {
        this.frameNr++;
        if (this.frameNr > this.framesCount) {
            this.frameNr = 1;
        }
        const frameXpos = (this.frameNr-1)*this.frameWidth;
        this.ctx.drawImage(this.image, frameXpos, frameYpos*this.frameHeight, this.frameWidth, this.frameHeight, this.x, this.y, this.frameWidth, this.frameHeight);
    }

    this.move = function() {
        this.x = this.x + this.stepX;
        this.y = this.y + this.stepY;
        this.draw();

        if(this.x+this.frameWidth >= 800 ) {this.stepX = -Math.abs(this.stepX); frameYpos = 1;}
        if(this.x <= 0) {this.stepX = Math.abs(this.stepX); frameYpos = 0;}

      

    }

    document.addEventListener("keydown",(e) =>{
    if(e.key == "ArrowLeft"){
        this.stepX = -Math.abs(this.stepX);
        this.direction = 1;
        frameYpos = 1;
        
    }
    if(e.key == "ArrowRight"){
        this.stepX = Math.abs(this.stepX);
        this.direction = 0;
        frameYpos = 0;

    }
    
    });

    


}

function drawAnim() {
    ctx.fillStyle = "#9999FF";
    ctx.fillRect(0, 0, canvasElem.width, canvasElem.height);

    for (let i=0; i<housesSmallCount; i++) {
        housesSmall[i][1] = housesSmall[i][1] - housesSmall[i][0];
        ctx.drawImage(houseS, housesSmall[i][1], 20);
        if (housesSmall[i][1] < -houseS.width) {
            housesSmall[i][1] = canvasElem.width;
            housesSmall[i][0] = 1 + Math.random();
        }
    }

    for (let j=0; j<housesBigCount; j++) {
        housesBig[j][1] = housesBig[j][1] - housesBig[j][0];
        ctx.drawImage(houseB, housesBig[j][1], 20);
        if (housesBig[j][1] < -houseB.width) {
            housesBig[j][1] = canvasElem.width;
            housesBig[j][0] = 2 + Math.random()
        }
    }

    human.move();
}
const canvasElem = document.querySelector('#canvasHuman')
const ctx = canvasElem.getContext('2d');

const houseB = new Image(245, 432);
houseB.src = 'd.png';

const houseS = new Image(350, 450);
houseS.src = 'd2.png';

const housesBigCount = 3;
const housesSmallCount = 5;

const housesBig = [];
const housesSmall = [];

for (let i=0; i<housesBigCount; i++) {
    housesBig.push([2+Math.random() , Math.random()*canvasElem.width])
}

for (let i=0; i<housesSmallCount; i++) {
    housesSmall.push([1+Math.random() , Math.random()*canvasElem.width])
}


ctx.canvas.width = 800;
ctx.canvas.height = 500;

    const humanImg = new Image(864, 280);
    humanImg.src = 'human.png';
    const human = new AnimatedObj(120, 360, 10, humanImg, 8, ctx,0);

  

 humanImg.addEventListener('load', function(){
    const animLoop = new FpsCtrl(24, drawAnim);
    animLoop.start();
});



});
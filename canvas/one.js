document.addEventListener("DOMContentLoaded",function(){

// const canvasElem = document.querySelector('canvas')
// const ctx = canvasElem.getContext('2d');
const canvasElem = document.getElementById('canvas');
const ctx = canvasElem.getContext('2d');

ctx.fillRect(0,0,150,150);
ctx.save(); //Zapisuje domyślny stan

ctx.fillStyle = "#09F"; //Robimy zmiany w ustawieniach
ctx.fillRect(15,15,120,120);

ctx.save(); //Zapisuje aktualny stan
ctx.fillStyle = '#FFF'
ctx.globalAlpha = 0.5;
ctx.fillRect(30,30,90,90);

ctx.restore(); //Przywraca poprzedni stan (zapisany w linijce 9)
ctx.fillRect(45,45,60,60);

ctx.restore(); //Przywraca domyślny stan (zapisany w linijce 4)
ctx.fillRect(60,60,30,30);

});
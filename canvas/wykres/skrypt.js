document.addEventListener('DOMContentLoaded',function(){

const up = document.querySelector("#up");
const down = document.querySelector("#down");
let cisnienie = [ 160, 100];
generateChart();

up.addEventListener("input",function(e){
    console.log(e.srcElement.value);
    cisnienie[0] = e.srcElement.value;
    generateChart();
})

down.addEventListener("input",function(e){
    console.log(e.srcElement.value);
    cisnienie[1] = e.srcElement.value;
    generateChart();
})


function generateChart(){

// dane wejściowe

let cisnienieRange = [[140,180],[90,110]];
let deviation = [60,30];
let pointerPos = [];

function setPointersPosition(){
    
    for(i=0;i<2;i++){

    if(cisnienie[i]<cisnienieRange[i][0]-deviation[i]) pointerPos[i]=x1b-10;
    else if(cisnienie[i]>0 && cisnienie[i]<cisnienieRange[i][0])
    pointerPos[i]=((cisnienie[i]-(cisnienieRange[i][0]-deviation[i]))/(deviation[i])*(x1e-x1b))+x1b-10;
    else if(cisnienie[i]<cisnienieRange[i][1]){
        pointerPos[i]=(cisnienie[i]-cisnienieRange[i][0])/(cisnienieRange[i][1]-cisnienieRange[i][0])*(x2-(x1e+lineBreak))+x1e+lineBreak-5;
    }else if(cisnienie[i] < cisnienieRange[i][1]+deviation[i]){
        pointerPos[i]=(cisnienie[i]-cisnienieRange[i][1])/deviation[i]*(110)+x2+lineBreak-10;
    }else
    {
        pointerPos[i]=x2+lineBreak+110-5;
    }


}
}

createCanvas = function() {
    const canvasElem = document.createElement('canvas');
    canvasElem.width = 1000;
    canvasElem.height = 100;
    return canvasElem;
}

const canvasCnt = document.querySelector("#wykresDiv");
canvasCnt.innerHTML = "";
const canvasElem = createCanvas();
canvasCnt.appendChild(canvasElem);
const ctx = canvasElem.getContext('2d');

let height = 10;
let lineBreak = 3;
let y = 25;
let x1b = 17;
let x1e = x1b+110;
let x2 = 227+x1e;
ctx.lineWidth = height;
ctx.lineJoin = 'round';


let lineColor = [ "#9ECEFF", "#0166DA", "#9ECEFF"];
setPointersPosition();
ctx.beginPath();
ctx.moveTo(x1e, y);
ctx.lineTo(x1b, y);
ctx.lineTo(x1e, y);
ctx.strokeStyle = lineColor[0];
ctx.stroke();

ctx.beginPath();
ctx.moveTo(x1e+lineBreak, y);
ctx.lineTo(x2, y);
ctx.strokeStyle = lineColor[1];
ctx.stroke();

ctx.beginPath();
ctx.moveTo(x2+lineBreak, y);
ctx.lineTo(x2+lineBreak+110, y);
ctx.lineTo(x2+lineBreak, y);
ctx.strokeStyle = lineColor[2];
ctx.stroke();

ctx.font = "13px Tahoma";
ctx.fillText(cisnienieRange[0][0] + " mm HG", x1e-25, y-12);
ctx.fillText(cisnienieRange[1][0] + " mm HG", x1e-23, y+12+height);
ctx.fillText(cisnienieRange[0][1] + " mm HG", x2-30, y-12);
ctx.fillText(cisnienieRange[1][1] + " mm HG", x2-30, y+12+height);

ctx.font = "8px Tahoma";
ctx.fillStyle = "white";

ctx.fillText("niskie", x1b+((x1e-x1b)/2)-10, y+(height/2)-2);
ctx.fillText("średnie", x1e+((x2-x1e)/2)-10, y+(height/2)-2);
ctx.fillText("wysokie", x2+((110)/2)-10, y+(height/2)-2);

ctx.lineWidth = 1.5;
let pointerRadius = 7;
let pointerX = pointerPos[1];
let pointerY = 39;

ctx.fillStyle = lineColor[1];
ctx.strokeStyle = "#FFF";
ctx.beginPath();
ctx.moveTo(pointerX, pointerY);
ctx.quadraticCurveTo(pointerX+pointerRadius, pointerY-25, pointerX+(2*pointerRadius), pointerY);
ctx.fill();
ctx.stroke();
ctx.beginPath();
ctx.arc(pointerX+pointerRadius, pointerY, pointerRadius, 0,  Math.PI);
ctx.fill();
ctx.stroke();

pointerX = pointerPos[0];
pointerY = 12;

ctx.fillStyle = lineColor[1];
ctx.strokeStyle = "#FFF";
ctx.beginPath();
ctx.moveTo(pointerX, pointerY);
ctx.quadraticCurveTo(pointerX+pointerRadius, pointerY+25, pointerX+(2*pointerRadius), pointerY);
ctx.fill();
ctx.stroke();
ctx.beginPath();
ctx.arc(pointerX+pointerRadius, pointerY, pointerRadius, Math.PI,  0);
ctx.fill();
ctx.stroke();

}
});
function myFun(par)
{
    console.log(par);
}

// myFun("ala");

function sumTab(tab){
    let sum = 0;
    if(arguments.length>0)
    {
        for(t of arguments[0])
        {
           sum+=t;
        }
    }

    console.log(sum);
}

//sumTab([1,2,3,4,5,6,7,8,9,100]);

//////////////////////////////////

function mixedText(text){
let num = 0;
let outText = "";
    for(l of text)
    {
        num = Math.random();
        if(num > 0.5) outText+=l.toUpperCase ();
        else outText+=l.toLowerCase();
    }

return outText;
}

//console.log(mixedText("Ala ma kota, a kot ma ale"));

function iloczyn(a,b){
    if(typeof a !== "number" || typeof b !== "number")
        return false;
    else
        return a*b;

}

// console.log(iloczyn("33d",343));

const tab = ['Marcia', 'Ania', 'Agnieszka'];


const newTab = tab.map(function(el) { //zwracam nową tablicę...
    return el.toUpperCase()
}).filter(function(el) { //więc mogę ją odfiltrować
    return el[el.length-1].toUpperCase() === "A"
}).map(function(el) { //map też może służyć do iteracji
    return el + "!";
}).forEach(function(el) {
    //console.log(el);
}) //forEach nie zwraca tablicy więc nie mogę już tutaj działać jak na tablicy

//console.log(newTab)

const tabUsers = [
    {name : "Marcin", age: 14},
    {name : "Piotr", age: 18},
    {name : "Agnieszka", age: 13},
    {name : "Weronika", age: 20}
]


const tab2 = tabUsers.filter(wiek => (wiek.age)>=18);

console.log(tab2);
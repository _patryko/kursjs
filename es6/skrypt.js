const translateToEN = {
    wordPL : ["kot", "pies", "mysz", "lis", "pszczoła"],
    wordEN : ["cat", "dog", "mouse", "fox", "bee"],
    translate(val) {
        if(this.wordPL.indexOf(val) !== -1){
           let ind = this.wordPL.indexOf(val);
           return this.wordEN[ind];
        }
        return "Brak słowa w słowniku"
    }

};

console.log(translateToEN.translate("cat"));

const ob2 = Object.assign({},translateToEN);

ob2.translate = function(val) {
    if(this.wordEN.indexOf(val) !== -1){

        let ind = this.wordEN.indexOf(val);
        return this.wordPL[ind];
    }
    return "Unknown word"

};

console.log(ob2.translate("bee"));

const tabUsers = [
    { name : 'Marcin', age: 18 },
    { name : 'Ania', age: 16 },
    { name : 'Agnieszka', age: 16}
];
const tabNr = [1, 2, 3];

const allMinors = tabUsers.every(el => el.age > 14);

console.log(allMinors);

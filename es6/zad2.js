/*
stwórz obiekt ob, który będzie miał:

- właściwość favoriteColors - tablica z kolorami (mogą być pisane słownie, mogą hexdecymalnie)
- metodę bind, w której pobierzesz ze strony jakiś element ( np. przycisk - jak go nie masz, dorób).

Po kliknięciu na pobrany element, stwórz w pętli nowe elementy:

- div o wymiarach 100x100
- border: 1px solid #333
- display:inline-block
- tło pobrane z tablicy favouriteColors

Stworzone elementy wstaw na stronę

*/

document.addEventListener("DOMContentLoaded",function(){


const ob = {
    favoriteColors : ["yellow", "red", "blue","green"],
    bind() {
        const div = document.querySelector(".testowy");
        const body = document.querySelector("body");
        div.addEventListener("click",e => {
            console.log("click");
            const newDiv = document.createElement("div");
            newDiv.style.height = "100px";
            newDiv.style.width = "100px";
            newDiv.style.border = "1px solid #333";
            newDiv.style.display = "inline-block";
            newDiv.style.backgroundColor =  this.favoriteColors[Math.floor(Math.random()*this.favoriteColors.length)];

            body.appendChild(newDiv);
        });
    }
}

ob.bind();

});
const tab1 = [1, 2, 3, 6, 5];
const tab2 = [10, 20, 15, 4];

const tab3 = [...tab1, ...tab2];

tab3.sort((a,b) => a-b);

console.log(Math.max(...tab3));
console.log(Math.min(...tab3));
console.log(tab3);


function printBig(...params) {
    console.log(params.join(" + ").toUpperCase());
};

wywołanie: printBig("pies", "świnka", "kot");



//////////////////////////
// Przykład ze strony
// const highlight = (parts, ...values) => {
//     return parts.reduce(
//         (allText, part) => `${allText}${values.length ? `<strong>${values.shift()}</strong>` : ''}${part}`);
// }

// const name = "Marcin";
// const dog = "Szamson";

// const text = highlight`Mam na imię ${name} a mój pies to ${dog}`

ob = {
    name : "Patryk",
    height : 179,
    calculateAge(){
        return (new Date().getFullYear() - 1997);
    }

}
modifyString = (parts,...val) => {
    newString = "";
        parts.forEach((item,i) => {
            newString+=item;
            if(val[i] !== undefined){
                newString+=`[${val[i]}]`;
            }
        })
    return newString;
    }

console.log(modifyString`Nazywam się ${ob.name}
Mój wzrost to ${ob.height}
Mam ${ob.calculateAge()} lat`);

//////////////////////////////////////////

const obj = {
    name : "Marcin",
    surname : "Kowalski",
    age : 20
}

const { name, surname, age : userAge } = obj;
console.log(name);
console.log(surname);
console.log(userAge);

const tab = [1,2,3,4,5];

const [a,b,,c] = tab;
console.log(a,b,c);


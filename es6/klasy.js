class Brick{
    constructor(){
        this.x=0;
        this.y=0;
        this.graphic="";
        this.width=0;
        this.height=0;
        this.type=0;
        this.live=0;
    }

    print(){
        console.log(this.x,this.y,this.graphic,this.width,this.height,this.type,this.live);
    }

    init(){
        console.log("Dodano na plansze");
    }

}

class BrickBlue extends Brick{
    constructor(){
        super();
        this.graphic="blue.png";
        this.live=10;
    }
}

class BrickRed extends Brick{
    constructor(){
        super();
        this.graphic="red.png";
        this.live=15;
    }
}
class BrickGreen extends Brick{
    constructor(){
        super();
        this.graphic="green.png";
        this.live=20;
    }
}

class BrickAnim extends Brick{
    constructor(){
        super();
        this.speed=100;
    }

    moveHorizontal(){
        console.log("Poruszam się poziomo z szybkością " + this.speed);
    }
}

const brick = new Brick();
const redBrick = new BrickRed();
const blueBrick = new BrickBlue();
const animBrick = new BrickAnim();

brick.init();
brick.print();

redBrick.init();
redBrick.print();

blueBrick.init();
blueBrick.print();

animBrick.init();
animBrick.print();
animBrick.moveHorizontal();
/*
Zadanie:
-------------

Przy wysyłce formularza dodaj do listy nowego użytkownika. Niech formularz nie przeładowuje strony.
Postaraj się nie podpinać zdarzenia click dla button:submit, a podpiąć się pod zdarzenie submit dla formularza.

Po kliknięciu na przycisk usuwania (kosz na śmieci) usuń danego użytkownika z listy
*/


document.addEventListener('DOMContentLoaded', function() {

let users_list = document.querySelector(".user-list");
let users = document.querySelectorAll(".user");

let form = document.querySelector(".form");

let button = form.querySelector(".btn[type=submit]");

form.addEventListener("submit",function(e){
    e.preventDefault();
    

    if(checkValidate()){
     addItem();
    }



})


checkValidate = function(){
    const rPhone = /^\d{9,}$/
    const rName = /^[a-z]{3,}\s[a-z]*$/gi
    if(rPhone.test((form.querySelector("#phone").value)) && rName.test(form.querySelector("#name").value)) return true;
    else return false;
}

addItem = function(){
    let elem = document.createElement("li");
    elem.classList.add("user");
    elem = users_list.appendChild(elem);
    let userNode = elem;
    //console.log(elem);

    const elemIn = document.createElement("div");
    elemIn.classList.add("user-data");
    elem = elem.appendChild(elemIn);
    

    const elemInUser = document.createElement("div");
    elemInUser.classList.add("user-name");
    elemInUser.innerText = form.querySelector("#name").value;
    elem.appendChild(elemInUser);


    const elemInPhone = document.createElement("div");
    elemInPhone.classList.add("user-phone");
    elemInPhone.innerText = form.querySelector("#phone").value;
    elem.appendChild(elemInPhone);

    const delButton = document.createElement("button");
    delButton.classList.add("btn");
    delButton.classList.add("user-delete");
    delButton.innerText = "Usuń";

    userNode.appendChild(delButton);

    form.querySelector("#phone").value = "";
    form.querySelector("#name").value = "";
}

users_list.addEventListener("click",function(e){
    
    e.preventDefault();
    
    if(e.target.classList.contains("user-delete"))
{
    let el = e.target;
    while (el.parentNode) {
        el = el.parentNode;
        if (el.classList.contains("user"))
            break;
    }
    users_list.removeChild(el);
}
});

});
/*
Zadanie:
-------------

Po kliknięciu w "dodaj element" do listy dodaj nowy element podobny do tego już istniejącego na liście
Po dodaniu powinien mieć kolejny numer który wylicz na podstawie ilości elementów + 1

Po kliknięciu w ikonę kosza usuń dany element
Po kliknięciu w ikonę klonowania sklonuj na koniec listy dany element

*/
document.addEventListener('DOMContentLoaded', function() {

    let lista = document.querySelector(".list");
    let elements = document.getElementsByClassName("element");

    console.log(elements.length);
    const but = document.querySelector("#add");
    const org = document.querySelector("#elementInner");
    
    but.addEventListener("click",function(e){
        const elem = document.createElement("div");
        elem.classList.add("element");
        elem.innerHTML = org.innerHTML;
        const ref = lista.appendChild(elem);
        ref.querySelector(".nr").innerText = elements.length;
        
    });

    lista.addEventListener("click",function(e){
        if(e.target.classList.contains("clone"))
        {
            cloneMe(e.target.parentNode);
        }
        if(e.target.classList.contains("delete"))
        {
            deleteMe(e.target.parentNode);
        }


    });

    function cloneMe(me){
        lista.appendChild(me.cloneNode(true));
    }

    function deleteMe(me){
        lista.removeChild(me);
    }




})
/*
Zadanie:
-------------
po kliknięciu na przycisk przy przy samochodzie:
- pokaż element .car-detail dla danego samochodu.
- zmień tekst na przycisku na "schowaj detale".
- dodaj klasę .car-show-detail do danego samochodu - elementu .car

Po ponownym kliknięciu na przycisk
- Przywróć początkowy tekst na przycisku (pokaż detale)
- schowaj .car-detail danego samochodu
- usuń klasę .car-show-detail z danego samochodu
*/
document.addEventListener('DOMContentLoaded', function() {

    list_of_cars = document.querySelector(".car-list");


    list_of_cars.addEventListener("click",function(e){


        if(e.target.classList.contains("car-toggle-detail")){
            let el = e.target;
            while (el.parentNode) {
                el = el.parentNode;
                if (el.classList.contains("car"))
                    break;
            }
            let dspl = el.lastElementChild.style.display;

            if(el.lastElementChild.style.display == "none"){

            el.lastElementChild.style.display = "flex";
            el.classList.add("car-show-detail");
            e.target.innerText = "Schowaj detale";
            }else{
            
            el.lastElementChild.style.display = "none";
            el.classList.remove("car-show-detail");
            e.target.innerText = "Pokaż detale";
            }
            
        };
    });
  
 
});
/*
Zadanie:
-------------

w pliku html mamy mapę.
Dołączone są też 2 skrypty: cities.js i app.js
W tym pierwszym jest tablica z miastami - nie musisz go ruszać, ale przejrzeć powinieneś.

Naszym zadaniem jest:

1)
zrobienie pętli po tej tablicy
i wygenerowanie dla każdego elementu tablicy elementu:

<a
    href="http://www.poznan.pl/"
    class="map-marker"
    data-name="Poznań"
    data-population="542689"
    style="left: 180px; top: 270px;"
>Poznań</a>

Wykorzystaj tutaj dataset, classList, style itp.

Wygenerowany element wrzuć do mapy. Wszystkie dane potrzebne do wygenerowania takiego elementu są w tablicy.

2)
Wygenerowanie elementu i wrzucenie go do body:
<div class="map-tooltip" style="left: -9999px; top: -9999px"></div>


3)
Złapanie wygenerowanych wcześniej markerów (.map-marker)
Dla każdego takiego markera podepnijmy zdarzenia:
a) mouseover:
    - niech wypełnia tooltipa następującym html:
        <h2>Nazwa miasta</h2>
        <div>Population: <strong>Populacja miasta</strong></div>
    - niech ustawia go w pozycji kursora myszki (dodajć + 30 do x i y)
    - niech pokazuje tooltipa
b) mousemove:
    - niech ustawia tooltipa w pozycji kursora myszki (dodać + 30 do x i y)
c) mouseout:
    - niech czyści html tooltipa ("")
    - niech ukrywa toolip
*/

document.addEventListener('DOMContentLoaded', function() {

    const mapa = document.querySelector(".map");
    let markery = [];

    cities.forEach(function(item){
        const ah = document.createElement("a");
        ah.setAttribute("href",item.href);
        ah.classList.add("map-marker");
        ah.dataset.name = item.name;
        ah.dataset.population = item.population;
        ah.style.left = item.map_x + "px";
        ah.style.top = item.map_y + "px";

        markery.push(mapa.appendChild(ah));
    });

    const div = document.createElement("div");
    div.classList.add("map-tooltip");
    div.style.left = "-9999px";
    div.style.top = "-9999px";

    const tooltip = mapa.appendChild(div);

    markery.forEach(function(item){
        item.addEventListener("mouseover",function(e){
            tooltip.innerHTML = `<h2>${item.dataset.name}</h2>
            <div>Population: <strong>${item.dataset.population}</strong></div>`;
            const rect = mapa.getBoundingClientRect();
            const x = e.pageX - (rect.left + window.scrollX);
            const y = e.pageY - (rect.top + window.scrollY);
            
            tooltip.style.left = x+30+"px";
            tooltip.style.top = y+30+"px";
        });

        item.addEventListener("mousemove",function(e){
            const rect = mapa.getBoundingClientRect();
            const x = e.pageX - (rect.left + window.scrollX);
            const y = e.pageY - (rect.top + window.scrollY);
            
            tooltip.style.left = x+30+"px";
            tooltip.style.top = y+30+"px";
        });

        item.addEventListener("mouseout",function(e){
            tooltip.innerHTML = "";
            tooltip.style.left = "-9999px";
            tooltip.style.top = "-9999px";
        });
    });

});
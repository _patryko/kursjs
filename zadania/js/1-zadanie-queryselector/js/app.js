//Rozwiązując kolejne punkty powinieneś z planszy usuwać kwadraty z danym numerem.
//Jeżeli dane kwadraty nie zostały usunięte, znaczy że polecenie nie zostało dobrze wykonane.

// 1. Znajdź elementy o klasie .first-attempt - dodaj im klasę active
// 2. Znajdź elementy z atrybutem data-border i dodaj im atrybut data-el-active. Wykorzystaj dataset i inny rodzaj pętli niż w 1 zadaniu
// 3. Znajdź elementy z klasą hack. Dodaj im atrybut title ustawiony na wartość "hacking". Ten atrybut nie ma mieć z przodu data-
// 4. Znajdź elementy o klasie hijack. Usuń im atrybut title
// 5. Znajdź elementy które mają 2 klasy równocześnie: st1 i st2. Dodaj im style: color: red, i font-size na 15px
// 6. Znajdź elementy które mają klasę .del ale nie mają klasy .hijack. Dodaj im atrybut data-hack-active, usuń atrybut data-hack-inactive
// 7. Znajdź elementy o klase .last-attempt i pokaż w ich wnętrzu spany

const doc = document.querySelectorAll('.first-attempt');

for(d of doc){
    d.setAttribute('class','active');
}


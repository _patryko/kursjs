//Rozwiązując kolejne punkty powinieneś z planszy usuwać kwadraty z danym numerem.
//Jeżeli dane kwadraty nie zostały usunięte, znaczy że polecenie nie zostało dobrze wykonane.

// 1. Znajdź elementy o klasie .first-attempt - dodaj im klasę active
const first = document.querySelectorAll('.first-attempt');

for(d of first){
    //d.setAttribute('class','active');
    d.className = "active";
}

// 2. Znajdź elementy z atrybutem data-border i dodaj im atrybut data-el-active. Wykorzystaj dataset i inny rodzaj pętli niż w 1 zadaniu
const d_bor = document.querySelectorAll('[data-border]');

for(i=0;i<d_bor.length;i++){
    d_bor[i].dataset.elActive = "";
}

// 3. Znajdź elementy z klasą hack. Dodaj im atrybut title ustawiony na wartość "hacking". Ten atrybut nie ma mieć z przodu data-
const hack = document.querySelectorAll('.hack');
for(h of hack){
    h.setAttribute('title','hacking');
}

// 4. Znajdź elementy o klasie hijack. Usuń im atrybut title
const hijack = document.querySelectorAll('.hijack');
for(h of hijack){
    h.removeAttribute("title");
}

// 5. Znajdź elementy które mają 2 klasy równocześnie: st1 i st2. Dodaj im style: color: red, i font-size na 15px
const elems = document.querySelectorAll('.st1 , .st2');
for(e of elems){
    e.setAttribute("style","color:red; font-size:15px;");
}

// 6. Znajdź elementy które mają klasę .del ale nie mają klasy .hijack. Dodaj im atrybut data-hack-active, usuń atrybut data-hack-inactive
const elems2 = document.querySelectorAll('.del, :not(.hijack)');
for(e of elems2){
    e.dataset.hackActive = "";
    e.removeAttribute("data-hack-inactive");
}

// 7. Znajdź elementy o klase .last-attempt i pokaż w ich wnętrzu spany
const elems3 = document.querySelectorAll('.last-attempt');
for(e of elems3){
    //e.setAttribute("style","display:none;")
    const sp = e.querySelector("span");
    sp.setAttribute("style","display:none;");
}










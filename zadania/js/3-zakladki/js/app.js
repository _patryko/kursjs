/*
Zadanie:
-------------

Po kliknięciu w link w zakładkach:
- przełącz aktywną zakładkę (zmieniając klasę tab-el-active)
- pokaż treść zakładki na którą kieruje dany link, ukryj pozostałe treści
*/



document.addEventListener('DOMContentLoaded', function() {

let li_list = document.querySelectorAll(".tab-el");
let currentTab = document.querySelector(".tab-el-active");
let currentContent,id;
li_list.forEach(function(item){
    item.addEventListener("click",function(e){

        currentTab.classList.toggle("tab-el-active");
        id = currentTab.firstElementChild.getAttribute("href").substr(1);
        currentContent = document.querySelector("#"+id);
        currentContent.classList.toggle("tab-content-active");

        currentTab = e.currentTarget;

        currentTab.classList.toggle("tab-el-active");
        id = currentTab.firstElementChild.getAttribute("href").substr(1);
        currentContent = document.querySelector("#"+id);
        currentContent.classList.toggle("tab-content-active");



    });
});

});
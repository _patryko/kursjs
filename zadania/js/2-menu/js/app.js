/*
Zadanie:
-------------

Po kliknięciu na LI przełącz klasę .nav-li-active z obecnie zaznaczonego elementu na element kliknięty.

Zwróć uwagę, że klasa .nav-li-active jest nadawana na element LI a nie na A. Postaraj się sprawić, by
podczas takiego kliku strona nie była przeładowywana.
*/

document.addEventListener('DOMContentLoaded', function() {
    const li_list = document.querySelectorAll(".nav-el");

    let currentActive = document.querySelector(".nav-el-active");

    li_list.forEach(function(item){
        item.addEventListener("click",function(event){
            event.preventDefault();
            currentActive.classList.remove("nav-el-active");
            currentActive = event.currentTarget;
            currentActive.classList.add("nav-el-active");
        })
    })
});

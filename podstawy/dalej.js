let text = "To jest zdanie do zadania pierwszego ;)";
const text2 = "Bardzo lubię jeść marchewkę";

let arr = text2.split(" ");
console.log(arr.length);

let name = "Patryk";

console.log(name.substr(0,1).toUpperCase() + name.substr(1));
// console.log(text + " -> " + text.length);

const email = "loremimpsu@mgmail.com";

if(email.indexOf("@") > -1){
    console.log("email zawiera @");
}else
{
    console.log("email nie zawiera @");
}


//////////////////////////////

 text = "Uczę się stringów w C++";

///////////////////

const tab = [1,2,3,4];

for (const el of tab) {
    console.log(el);
}


const buttons = document.querySelectorAll('button');
for (const btn of buttons) {
    console.log(btn);
}


const txt = "ALa ma kota";
for (const i of txt) {
    console.log(i.toUpperCase());
}

function sum() {
    console.log(arguments);
}

sum(); //[] (zobacz dokładniej co wyszło w konsoli, bo wynik nie jest dokładnie taki)
sum(1,2,3,4); //[1,2,3,4]
sum("ala", "ma", "kota"); //["ala", "ma", "kota"]

function calculate() {
    if (arguments.length < 2) {
        console.warn('Błąd: Musisz podać minimum 2 liczby');
    } else {
        let result = 0;
        for (let i=0; i<arguments.length; i++) {
            result += arguments[i];
        }
        console.log(result);
    }
}

calculate(); //Wypisze "Błąd: Musisz podać minimum 2 liczby"
calculate(3); //Wypisze "Błąd: Musisz podać minimum 2 liczby"
calculate(4, 5, 6); //Wypisze 15
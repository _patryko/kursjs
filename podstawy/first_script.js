let num = 123;
let text = "Tekst";
let bol = true;
let un = undefined;
let nu = null;

let ar = [1,2,3];
let ob = {};

console.log(num + text);
console.log(num + bol);
console.log(num + un);
console.log(num + nu);
console.log(un + nu);
console.log(ar + ob);
console.log(ar + bol);

let width = "20px";

console.log(parseInt(width,10) + 30);

let age = 3;
if(typeof age === "undefined"){
    let age;
    console.log("Age " + age);
}